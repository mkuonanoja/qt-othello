/*
 * Author: Matti Kuonanoja (2015)
 *
 */

import QtQuick 2.0
import othellogame 1.0

Rectangle {
    id: root

    property int stone: 0
    property bool isLegalMove: false

    signal clicked()

    color: "#ff0000"
    border.color: "#000000"
    border.width: 20

    Image {
        id: backroundImage
        source: "images/tile.png"
        smooth: true
        anchors.fill: parent
    }

    Rectangle {
        id: highlightElement
        color: "#40ffffff"
        visible : root.isLegalMove
        anchors.fill: parent
    }

    onStoneChanged: {
        switch(root.stone) {
        case 0: backroundImage.source = "images/tile.png";
            break;
        case 1: backroundImage.source = "images/black_tile.png";
            break;
        case 2: backroundImage.source = "images/white_tile.png";
            break;
        }
    }

    MouseArea {
        id: mouseArea
        hoverEnabled: true
        anchors.fill: parent

        onClicked: {
            root.clicked();
        }
    }
}
