import QtQuick 2.0

Item {
    id: root

    property string text
    signal clicked()

    width: childrenRect.width
    height: childrenRect.height

    Text {
        id: buttonText
        text: root.text
        color: "#ffffff"
        font.pointSize: 20
        font.family: "Bodoni MT"
        //anchors.centerIn: parent
        //anchors.fill: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked: root.clicked()
    }
}

