/*
 * Author: Matti Kuonanoja (2015)
 *
 */

import QtQuick 2.0
import othellogame 1.0

Rectangle {
    id: root

    property OthelloGame gameObject

    signal tileClicked(int x, int y)
    signal checkLegalMoves(int nextStone)
    signal setTile(int x, int y, int stone)

    color: "#000000"

    ListModel {
        id: tileModel

        function createTileItems() {
            for (var y = 0; y < 8; ++y) {
                for(var x = 0; x < 8; ++x) {
                    var tile = {};
                    tile.tileX = x;
                    tile.tileY = y;
                    tile.tileStone = gameObject.getStoneAt(x, y);
                    tile.tileLegalMove = gameObject.isLegalMove(x, y, gameObject.getNextStone());
                    tileModel.append(tile);
                }
            }
        }

        function updateLegalMoves(nextStone) {
            for (var i = 0; i < tileModel.count; ++i) {
                var item = tileModel.get(i);
                var isLegal = gameObject.isLegalMove(item.tileX, item.tileY, nextStone);
                tileModel.setProperty(i, "tileLegalMove", isLegal);
            }
        }

        Component.onCompleted: {
            createTileItems();
        }
    }

    Grid {
        id: tileGrid

        spacing: 0
        columns: 8
        rows: 8
        anchors.fill: parent

        Repeater {
            id: tileRepeater

            model: tileModel
            delegate: Tile {
                stone: tileStone
                width: root.width / 8
                height: root.height / 8
                isLegalMove: tileLegalMove

                onClicked: {
                    root.tileClicked(tileX, tileY);
                }
            }
        }
    }

    onSetTile: {
        var index = x + y * 8;
        if (index >= 0 && index < tileModel.count) {
            var tileItem = tileModel.get(index);
            tileModel.setProperty(index, "tileStone", stone)
        }
    }

    onCheckLegalMoves: {
        tileModel.updateLegalMoves(nextStone);
    }
}
