/*
 * Author: Matti Kuonanoja (2015)
 *
 */

import QtQuick 2.4
import QtQuick.Window 2.2
import othellogame 1.0

Window {
    id: root
    title: qsTr("Othello Game")
    width: 400
    height: 640
    visible: true

    Rectangle {
        id: background
        anchors.fill: parent

        gradient: Gradient {
            GradientStop { position: 0.00; color: "#602D04" }
            GradientStop { position: 0.15; color: "#602D04" }
            GradientStop { position: 0.85; color: "#3C1C02" }
            GradientStop { position: 1.00; color: "#3C1C02" }
        }

        StateInfo {
            id: stateInfo
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: 50

            blackStoneCount: gameObject.blackStoneCount
            whiteStoneCount: gameObject.whiteStoneCount
            turnNumber: gameObject.stoneCount - 3
            gameOver: gameObject.gameOver
        }

        Rectangle {
            id: turnIndicator
            width: 70
            height: 6
            radius: 2
            color: "#FAC090"
            anchors.top: stateInfo.bottom
            anchors.topMargin: 4
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            visible: !gameObject.gameOver
            state: gameObject.nextStone == OthelloGame.BLACK_STONE ? "left" : "right"

            states: [
                State {
                    name: "left"
                    AnchorChanges { target: turnIndicator; anchors.left: background.left  }
                    AnchorChanges { target: turnIndicator; anchors.left: background.left  }
                },
                State {
                    name: "right"
                    AnchorChanges { target: turnIndicator; anchors.right: background.right  }
                }
            ]

            transitions: Transition {
                AnchorAnimation { duration: 1000; easing.type: Easing.InOutQuad  }
            }
        }

        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: stateInfo.bottom
            anchors.bottom: board.top

            Text {
                id: stateText
                text: {
                    if (gameObject.gameOver) {
                        if (gameObject.blackStoneCount === gameObject.whiteStoneCount){
                            return "Tie"
                        } else {
                            if (gameObject.blackStoneCount > gameObject.whiteStoneCount) {
                                return "Black player won";
                            } else {
                                return "White player won"
                            }
                        }
                    } else {
                        return gameObject.nextStone === OthelloGame.BLACK_STONE ? "Black player's turn" : "White player's turn.";
                    }
                }
                color: "#FAC090"
                font.pointSize: 20
                font.family: "Bodoni MT"
                anchors.centerIn: parent
            }
        }

        Board {
            id: board
            gameObject: gameObject
            width: 360
            height: 360
            anchors.centerIn: parent

            onTileClicked: {
                if (gameObject.isLegalMove(x, y, gameObject.getNextStone())) {
                    gameObject.makeMove(x, y, gameObject.getNextStone());
                    checkLegalMoves(gameObject.nextStone);
                } else {
                    console.log("Illegal move: " + x + " " + y);
                }
            }
            Component.onCompleted: checkLegalMoves(gameObject.nextStone);
        }

        Item {
            id: restartBar
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: board.bottom
            anchors.bottom: buttonBar.top

            Button {
                id: restartButton
                text: "Start a new game"
                anchors.centerIn: parent
                onClicked: {
                    gameObject.reset();
                    board.checkLegalMoves(gameObject.nextStone);
                }
            }
        }

        Item {
            id: buttonBar
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height: 50

            Button {
                id: exitButton
                text: "Quit"
                anchors.right: parent.right
                anchors.rightMargin: 30
                anchors.verticalCenter: parent.verticalCenter
                onClicked: {
                    Qt.quit();
                }
            }
        }

        OthelloGame {
            id: gameObject

            onStoneAdded: {
                board.setTile(x, y, stone);
            }

            onStoneFlipped: {
                board.setTile(x, y, stone);
            }

            onStoneRemoved: {
                board.setTile(x, y, OthelloGame.EMPTY);
            }
        }
    }

    Component.onCompleted: {
        board.checkLegalMoves(gameObject.nextStone);
    }
}
