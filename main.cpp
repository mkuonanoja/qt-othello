/*
 * Author: Matti Kuonanoja (2015)
 *
 */

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include "othellogame.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<OthelloGame>("othellogame", 1, 0, "OthelloGame");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
