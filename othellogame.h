/*
 * Author: Matti Kuonanoja (2015)
 *
 */

#ifndef OTHELLOGAME_H
#define OTHELLOGAME_H

#include <QObject>
#include <QVector>
#include <QSharedPointer>

class OthelloMove;

class OthelloGame : public QObject
{
    Q_OBJECT
    Q_ENUMS(Stone)
    Q_PROPERTY(int stoneCount READ getStoneCount NOTIFY stoneCountChanged)
    Q_PROPERTY(int blackStoneCount READ getBlackStoneCount NOTIFY blackStoneCountChanged)
    Q_PROPERTY(int whiteStoneCount READ getWhiteStoneCount NOTIFY whiteStoneCountChanged)
    Q_PROPERTY(Stone nextStone READ getNextStone NOTIFY nextStoneChanged)
    Q_PROPERTY(bool gameOver READ isGameOver NOTIFY gameOverChanged)

public:
    explicit OthelloGame(QObject *parent = 0);
    ~OthelloGame();

     enum Stone { EMPTY = 0, BLACK_STONE, WHITE_STONE };

signals:
     void stoneAdded(int x, int y, Stone stone);
     void stoneRemoved(int x, int y, Stone stone);
     void stoneFlipped(int x, int y, Stone stone);
     void stoneCountChanged(int stoneCount);
     void nextStoneChanged(Stone nextStone);
     void gameOverChanged(bool gameOver);
     void blackStoneCountChanged(int blackStoneCount);
     void whiteStoneCountChanged(int whiteStoneCount);

public slots:
    void reset();
    void makeMove(int x, int y, Stone stone);

    int getStoneCount() const;
    int getBlackStoneCount() const;
    int getWhiteStoneCount() const;
    Stone getNextStone() const;
    Stone getStoneAt(int x, int y) const;
    bool isLegalMove(int x, int y, Stone stone) const;
    QVector<QSharedPointer<OthelloMove>> getLegalMoves(Stone stone) const;
    bool isGameOver() const;

private:
    static int getRelativePositionX(int baseX, int direction, int distance);
    static int getRelativePositionY(int baseY, int direction, int distance);

    void setStone(int x, int y, Stone stone);
    void setStones(int x, int y, int direction, int count, Stone stone);
    void flipStones(int newStoneX, int newStoneY, Stone newStone);
    void changeToNextStone();

    int getFlipCount(int x, int y, int direction, Stone stone) const;

    QVector<Stone> stones;
    Stone nextStone;
    int stoneCount;
};

#endif // OTHELLOGAME_H
