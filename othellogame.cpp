/*
 * Author: Matti Kuonanoja (2015)
 *
 */

#include "othellogame.h"
#include "othellomove.h"

OthelloGame::OthelloGame(QObject *parent) :
    QObject(parent),
    stones(64),
    nextStone(BLACK_STONE),
    stoneCount(0)
{
    reset();
}

OthelloGame::~OthelloGame()
{

}

void OthelloGame::reset()
{
    for (int x = 0; x < 8; ++x) {
        for (int y = 0; y < 8; ++y) {
            setStone(x, y, EMPTY);
        }
    }

    setStone(3, 3, BLACK_STONE);
    setStone(3, 4, WHITE_STONE);
    setStone(4, 3, WHITE_STONE);
    setStone(4, 4, BLACK_STONE);

    nextStone = BLACK_STONE;

    emit nextStoneChanged(nextStone);
    emit gameOverChanged(false);
}

void OthelloGame::makeMove(int x, int y, OthelloGame::Stone stone)
{
    if (isLegalMove(x, y, stone)) {
        flipStones(x, y, stone);
        setStone(x, y, stone);
        changeToNextStone();
    }
}

int OthelloGame::getStoneCount() const
{
    return stoneCount;
}

int OthelloGame::getBlackStoneCount() const
{
    int stoneCount = 0;
    for (int x = 0; x < 8; ++x) {
        for (int y = 0; y < 8; ++y) {
            if (getStoneAt(x,y) == BLACK_STONE)
                stoneCount++;
        }
    }
    return stoneCount;
}

int OthelloGame::getWhiteStoneCount() const
{
    int stoneCount = 0;
    for (int x = 0; x < 8; ++x) {
        for (int y = 0; y < 8; ++y) {
            if (getStoneAt(x,y) == WHITE_STONE)
                stoneCount++;
        }
    }
    return stoneCount;
}

OthelloGame::Stone OthelloGame::getNextStone() const
{
    return nextStone;
}

OthelloGame::Stone OthelloGame::getStoneAt(int x, int y) const
{
    int index = x + 8 * y;
    if (index >= 0 && index < 64) {
        return stones[index];
    } else {
        return EMPTY;
    }
}

bool OthelloGame::isLegalMove(int x, int y, OthelloGame::Stone stone) const
{
    if (stone == EMPTY) {
        return false;
    }

    if (x < 0 || x >= 8 || y < 0 || y >= 8) {
        return false;
    }

    if (getStoneAt(x,y) != EMPTY) {
        return false;
    }

    int totalFlipCount = 0;
    for (int direction = 0; direction < 8; ++direction) {
        int flipCount = getFlipCount(x, y, direction, stone);
        totalFlipCount += flipCount;
    }
    if (totalFlipCount > 0)
        return true;
    else
        return false;
}

QVector<QSharedPointer<OthelloMove> > OthelloGame::getLegalMoves(Stone stone) const
{
    QVector<QSharedPointer<OthelloMove>> legalMoves;
    for (int x = 0; x < 8; ++x) {
        for (int y = 0; y < 8; ++y) {
            if (isLegalMove(x, y, stone)) {
                QSharedPointer<OthelloMove> move = QSharedPointer<OthelloMove>(new OthelloMove(x, y, stone));
                legalMoves.append(move);
            }
        }
    }
    return legalMoves;
}

bool OthelloGame::isGameOver() const
{
    if (getLegalMoves(WHITE_STONE).size() == 0 && getLegalMoves(BLACK_STONE).size() == 0) {
        return true;
    } else {
        return false;
    }
}

int OthelloGame::getRelativePositionX(int baseX, int direction, int distance)
{
    static int offsetX [] = { 1,  1,  0, -1, -1, -1, 0, 1 };
    return baseX + offsetX[direction % 8] * distance;
}

int OthelloGame::getRelativePositionY(int baseY, int direction, int distance)
{
    static int offsetY [] = { 0, -1, -1, -1,  0,  1, 1, 1 };
    return baseY + offsetY[direction % 8] * distance;
}

void OthelloGame::setStone(int x, int y, OthelloGame::Stone stone)
{
    int index = x + 8 * y;
    if (index >= 0 && index < 64) {
        Stone previousStone = stones[index];
        stones[index] = stone;

        if (stone != EMPTY) {
            if (previousStone == EMPTY) {
                stoneCount++;
                if (stone == BLACK_STONE)
                    emit blackStoneCountChanged(getBlackStoneCount());
                else
                    emit whiteStoneCountChanged(getWhiteStoneCount());
                emit stoneAdded(x, y, stone);
                emit stoneCountChanged(stoneCount);
            } else {
                if (previousStone != stone) {
                    emit stoneFlipped(x, y, stone);
                }
                emit blackStoneCountChanged(getBlackStoneCount());
                emit whiteStoneCountChanged(getWhiteStoneCount());
            }
        } else {
            if (previousStone != EMPTY) {
                stoneCount--;
                if (stone == BLACK_STONE)
                    emit blackStoneCountChanged(getBlackStoneCount());
                else
                    emit whiteStoneCountChanged(getWhiteStoneCount());
                emit stoneRemoved(x, y, previousStone);
            }
        }
    }
}

void OthelloGame::setStones(int x, int y, int direction, int count, Stone stone)
{
    for (int distance = 1; distance <= count; ++distance) {
        int relativeX = getRelativePositionX(x, direction, distance);
        int relativeY = getRelativePositionY(y, direction, distance);
        setStone(relativeX, relativeY, stone);
    }
}

void OthelloGame::flipStones(int newStoneX, int newStoneY, Stone newStone)
{
    for (int direction = 0; direction < 8; ++direction) {
        int flipCount = getFlipCount(newStoneX, newStoneY, direction, newStone);
        setStones(newStoneX, newStoneY, direction, flipCount, newStone);
    }
}

void OthelloGame::changeToNextStone()
{
    Stone opponentStone = (nextStone == BLACK_STONE) ? WHITE_STONE : BLACK_STONE;
    if (getLegalMoves(opponentStone).size() == 0 && getLegalMoves(nextStone).size() == 0) {
        // game over
        nextStone = EMPTY;
        emit gameOverChanged(true);
        emit nextStoneChanged(nextStone);
    } else {
        if (getLegalMoves(opponentStone).size() > 0) {
            nextStone = opponentStone;
            emit nextStoneChanged(opponentStone);
        }
    }
}

int OthelloGame::getFlipCount(int x, int y, int direction, Stone stone) const
{
    Stone oppenentStone = (stone == BLACK_STONE) ? WHITE_STONE : BLACK_STONE;
    int opponentStoneCount = 0;
    bool ownStoneFound = false;
    bool emptyTileFound = false;
    for (int distance = 1; distance < 8 && !ownStoneFound && !emptyTileFound; ++distance) {
        Stone relativeStone = EMPTY;

        int relativeX = getRelativePositionX(x, direction, distance);
        int relativeY = getRelativePositionY(y, direction, distance);
        if (relativeX >= 0 && relativeX < 8 && relativeY >= 0 && relativeY < 8) {
            relativeStone = getStoneAt(relativeX, relativeY);
        }

        if (relativeStone == oppenentStone) {
            ++opponentStoneCount;
        } else {
            ownStoneFound = (relativeStone == stone);
            emptyTileFound = (relativeStone == EMPTY);
        }
    }
    if (ownStoneFound)
        return  opponentStoneCount;
    else
        return 0;
}
