Othello game implementation using Qt, C++ and QML.

Project goal is to learn Qt, C++ and QML while implementing Othello AI algorithm and nice user interface.

Author: Matti Kuonanoja (2015)
