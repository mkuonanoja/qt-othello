/*
 * Author: Matti Kuonanoja (2015)
 *
 */

#ifndef OTHELLOMOVE_H
#define OTHELLOMOVE_H

#include <QObject>
#include "othellogame.h"

class OthelloMove : public QObject
{
    Q_OBJECT
public:
    explicit OthelloMove(QObject *parent = 0);
    OthelloMove(int x, int y, OthelloGame::Stone stone);
    ~OthelloMove();

    inline int getX() const { return x; }
    inline int getY() const { return y; }
    inline OthelloGame::Stone getStone() const { return stone; }

signals:

public slots:

private:
    int x;
    int y;
    OthelloGame::Stone stone;
};

#endif // OTHELLOMOVE_H
