/*
 * Author: Matti Kuonanoja (2015)
 *
 */

#include "othellomove.h"

OthelloMove::OthelloMove(QObject *parent) :
    QObject(parent),
    x(0),
    y(0),
    stone(OthelloGame::BLACK_STONE)
{

}

OthelloMove::OthelloMove(int x, int y, OthelloGame::Stone stone)
{
    this->x = x;
    this->y = y;
    this->stone = stone;
}

OthelloMove::~OthelloMove()
{

}
