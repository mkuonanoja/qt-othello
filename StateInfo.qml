import QtQuick 2.0
import othellogame 1.0

Item {
    id: root
    property int blackStoneCount
    property int whiteStoneCount
    property int nextStone
    property int turnNumber
    property bool gameOver: false

    Image {
        id: blackStoneImage
        source: "images/black_stone.png"
        width: 40
        height: 40
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
    }

    Text {
        id: blackStoneCountText
        text: blackStoneCount
        color: "#FAC090"
        font.pointSize: 24
        font.family: "Bodoni MT"
        anchors.left: blackStoneImage.right
        anchors.leftMargin: 10
        anchors.verticalCenter: blackStoneImage.verticalCenter
    }

    Text {
        id: turnText
        text: root.gameOver ? "Game over" : "Turn " + turnNumber
        color: "#FAC090"
        font.pointSize: 20
        font.family: "Bodoni MT"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: blackStoneImage.verticalCenter
    }

    Text {
        id: whiteStoneCountText
        text: whiteStoneCount
        color: "#FAC090"
        font.pointSize: 24
        font.family: "Bodoni MT"
        anchors.right: whiteStoneImage.left
        anchors.rightMargin: 10
        anchors.verticalCenter: blackStoneImage.verticalCenter
    }

    Image {
        id: whiteStoneImage
        source: "images/white_stone.png"
        width: 40
        height: 40
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
    }
}

