qTEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    othellogame.cpp \
    othellomove.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    othellogame.h \
    othellomove.h

DISTFILES +=
